# Phylogenomics of T4 cyanophages: Lateral gene transfer in the 'core' and origins of host genes #

## Author ##

Julio Cesar Ignacio-Espinoza

## CITATION ##

If you use this software, please cite [Ignacio-Espinoza, J.C., & Sullivan, M.B. (2012). Phylogenomics of T4 cyanophages: Lateral gene transfer in the 'core' and origins of host genes. Environ Microbiol. 14(8), 2113-2126.](http://onlinelibrary.wiley.com/doi/10.1111/j.1462-2920.2012.02704.x/abstract)