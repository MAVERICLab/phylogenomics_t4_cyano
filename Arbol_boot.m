function D = Arbol_boot(M, NAMES); %% M = Matrix of presence absence; NAMES = vetor with taxa names.
for i = 1:100                 %%
    r = randi (1438, 1438, 1);       %% Size of the matrix
    cnames = NAMES;
    S = M(:,r);
    D = 1- Simpson(M);
    tree_name = i;
    NJtree = seqneighjoin(D,'equivar',cnames);
    phytreewrite ('file_temp', NJtree);
    text = fileread('file_temp')
    fid = fopen('TREES_U.txt','a'); 
    fprintf(fid,'%s',text); 
    fclose(fid); 
    line = '\n';
    fid = fopen('TREES_U.txt','a');        %%  Bootsrap file is saved here
    fprintf(fid,'%s',line); 
    fclose(fid); 
end
return;
