function Mat = Simpson(M)
    Mat = zeros(27,27);
    for row1 = 1:27
        for row2 = 1:27
            both =0;
            first =0;
            second =0;
            for c = 1:2876
                if M(row1,c)>= 1 & M(row2,c)>=1
                    both = both+1;
                end
                if M(row1,c)>= 1
                    first = first +1;
                end
                if M(row2,c)>=1
                    second = second +1;
                end
            end
            if  first < second
                den = first;
            else
                den = second;
            end
            Mat(row1,row2)= both/den;
        end
    end
return
val;
